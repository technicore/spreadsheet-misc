Option Explicit

Public Interval As Double, T0 As Double
Public LastCalcTime As Currency
Public Ticks As Long, IdleCounter As Long
Public NameT As Name, NameTimeCounter As Name, NameCalcTime As Name, NameLastIdleCounter As Name
Public chkAnimationRun As ControlFormat
Public AnimationActive As Boolean

Public Sub AnimationUpdate()
    Dim TCalc As Currency
    mdlCalcPerf.LoadPerfFrequency
    TCalc = mdlCalcPerf.PerformanceCounter
    
    Ticks = Ticks + 1
    Application.ScreenUpdating = False
    Application.Calculation = xlCalculationManual
    If Not NameT Is Nothing Then NameT.RefersTo = T0 + Ticks
    If Not NameTimeCounter Is Nothing Then NameTimeCounter.RefersTo = TCalc * 10000 * mdlCalcPerf.InvFreq
    If Not NameCalcTime Is Nothing Then NameCalcTime.RefersTo = LastCalcTime * 10000 * mdlCalcPerf.InvFreq
    If Not NameLastIdleCounter Is Nothing Then NameLastIdleCounter.RefersTo = IdleCounter
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    
    LastCalcTime = mdlCalcPerf.PerformanceCounter - TCalc
    'DoEvents
End Sub

Sub SendAnimStop()
    If Not chkAnimationRun Is Nothing Then chkAnimationRun.Value = 0
    Application.OnKey "{ESCAPE}" 'Note: also add this line in Workbook_BeforeClose.
End Sub

Public Sub Animate()
    If AnimationActive Then Exit Sub
    On Error GoTo Finally
    Application.EnableCancelKey = xlErrorHandler
    Application.OnKey "{ESCAPE}", "SendAnimStop"
    AnimationActive = True
    
    'ThisWorkbook.Sheets("manager").EnableCalculation = False
    'ThisWorkbook.Sheets("world_vertices").EnableCalculation = False
    'ThisWorkbook.ForceFullCalculation = True
    Dim TNext As Currency
    mdlCalcPerf.LoadPerfFrequency
    Do While chkAnimationRun.Value > 0
        TNext = CCur(mdlCalcPerf.PerformanceCounter() + Interval * CDbl(mdlCalcPerf.cyFrequency))
        AnimationUpdate
        IdleCounter = 0
        Do
            DoEvents
            IdleCounter = IdleCounter + 1
        Loop While mdlCalcPerf.PerformanceCounter() < TNext
    Loop
Finally:
    AnimationActive = False
    chkAnimationRun.Value = 0
    'ThisWorkbook.Sheets("manager").EnableCalculation = True
    'ThisWorkbook.Sheets("world_vertices").EnableCalculation = True
    'ThisWorkbook.ForceFullCalculation = False
    'If Err = 18 Then 'cancelled by user
    'Err.Raise
End Sub
