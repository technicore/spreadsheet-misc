Attribute VB_Name = "mdlGamepad"
Option Explicit

'https://learn.microsoft.com/en-us/windows/win32/api/joystickapi/
'Note: not all WinAPI joystick APIs are implemented here.
Public Type JOYINFO
    wXpos As Long
    wYpos As Long
    wZpos As Long
    wButtons As Long
End Type

Public Type JOYINFOEX
    dwSize As Long
    dwFlags As Long
    dwXpos As Long
    dwYpos As Long
    dwZpos As Long
    dwRpos As Long
    dwUpos As Long
    dwVpos As Long
    dwButtons As Long
    dwButtonNumber As Long
    dwPOV As Long
    dwReserved1 As Long
    dwReserved2 As Long
End Type

Public Type JOYCAPSW 'to do: test this structure to see if it uses ucs-2/wide chars
    wMid As Integer
    wPid As Integer
    szPname As String * 32
    wXmin As Long
    wXmax As Long
    wYmin As Long
    wYmax As Long
    wZmin As Long
    wZmax As Long
    wNumButtons As Long
    wPeriodMin As Long
    wPeriodMax As Long
    wRmin As Long
    wRmax As Long
    wUmin As Long
    wUmax As Long
    wVmin As Long
    wVmax As Long
    wCaps As Long
    wMaxAxes As Long
    wNumAxes As Long
    wMaxButtons As Long
    szRegKey As String * 32
    szOEMVxD As String * 260
End Type

#If VBA7 Then
    Public Declare PtrSafe Function joyGetPos Lib "Winmm" (ByVal uJoyID As Long, pji As JOYINFO) As Long
    Public Declare PtrSafe Function joyGetPosEx Lib "Winmm" (ByVal uJoyID As Long, pji As JOYINFOEX) As Long
    'question: does the HWND type (or other handles) have the same size as a pointer?
    Public Declare PtrSafe Function joySetCapture Lib "Winmm" (ByVal hwnd As LongPtr, ByVal uJoyID As Long, ByVal uPeriod As Long, ByVal fChanged As Boolean) As Long
    Public Declare PtrSafe Function joyReleaseCapture Lib "Winmm" (ByVal uJoyID As Long) As Long
    Public Declare PtrSafe Function joyConfigChanged Lib "Winmm" (ByVal dwFlags As Long) As Long
    Public Declare PtrSafe Function joyGetNumDevs Lib "Winmm" () As Long
    Public Declare PtrSafe Function joyGetDevCapsW Lib "Winmm" (ByVal uJoyID As Long, pjc As JOYCAPSW, ByVal cbjc As Long) As Long
#Else
    Public Declare Function joyGetPos Lib "Winmm" (ByVal uJoyID As Long, pji As JOYINFO) As Long
    Public Declare Function joyGetPosEx Lib "Winmm" (ByVal uJoyID As Long, pji As JOYINFOEX) As Long
    Public Declare Function joySetCapture Lib "Winmm" (ByVal hwnd As Long, ByVal uJoyID As Long, ByVal uPeriod As Long, ByVal fChanged As Boolean) As Long
    Public Declare Function joyReleaseCapture Lib "Winmm" (ByVal uJoyID As Long) As Long
    Public Declare Function joyConfigChanged Lib "Winmm" (ByVal dwFlags As Long) As Long
    Public Declare Function joyGetNumDevs Lib "Winmm" () As Long
    Public Declare Function joyGetDevCapsW Lib "Winmm" (ByVal uJoyID As Long, pjc As JOYCAPSW, ByVal cbjc As Long) As Long
#End If
