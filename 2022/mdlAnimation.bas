Attribute VB_Name = "mdlAnimation"
Option Explicit
'to do: the scope of variables and statements may be wrong. check it.

'Public Interval As Date
Public Interval As Double, LastCalcTime As Double
Public T0 As Double
Public Ticks As Integer
Public NameT As Name, NameOutput As Name, NameCalcTime As Name
Public chkAnimationRun As ControlFormat, lblPerformance As Shape
Public AnimationActive As Boolean

Public Sub AnimationUpdate()
    'Dim TCalc As Double
    Dim ProcTimeSum As Currency, IntTimeUnbiased As Currency, ProcCycles As Currency
    'Dim PerfCounter As Currency, FileTime As Currency
    'TCalc = Timer
    ProcTimeSum = mdlCalcPerf.ProcessTimeSum()
    IntTimeUnbiased = mdlCalcPerf.InterruptTimeUnbiased()
    'mdlCalcPerf.LoadPerfFrequency
    'PerfCounter = mdlCalcPerf.PerformanceCounter()
    'FileTime = mdlCalcPerf.FileTimeAsCurrency()
    ProcCycles = mdlCalcPerf.ProcessCycleTime()

    'NameOutput.RefersTo = Rnd
    Ticks = Ticks + 1
    Application.ScreenUpdating = False
    Application.Calculation = xlCalculationManual
    NameT.RefersTo = T0 + Ticks
    'NameCalcTime.RefersTo = LastCalcTime
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic

    'LastCalcTime = Timer - TCalc
    ProcTimeSum = mdlCalcPerf.ProcessTimeSum() - ProcTimeSum
    IntTimeUnbiased = mdlCalcPerf.InterruptTimeUnbiased() - IntTimeUnbiased
    'PerfCounter = mdlCalcPerf.PerformanceCounter() - PerfCounter
    'FileTime = mdlCalcPerf.FileTimeAsCurrency() - FileTime
    ProcCycles = mdlCalcPerf.ProcessCycleTime() - ProcCycles

    Dim Status As String
    Status = "Calculation performance:" & vbCrLf _
        & "Process time (kernel + user): " & CStr(ProcTimeSum) & " ms" & vbCrLf _
        & "Unbiased interrupt time: " & CStr(IntTimeUnbiased) & " ms" & vbCrLf _
        & "Process CPU cycles: " & Format$(ProcCycles * 10000, "0.0###e-0")
        '& "Process CPU cycles: " & FormatNumber(ProcCycles * 10000, 0, vbTrue, vbFalse, vbTrue)
    If Application.Iteration Then
        Status = Status & vbCrLf & "Iteration: up to " & CStr(Application.MaxIterations) & " step(s)"
    Else
        Status = Status & vbCrLf & "Iteration: off"
    End If
    If Not lblPerformance Is Nothing Then lblPerformance.TextFrame.Characters().Text = Status
    'DoEvents
End Sub

'deprecated: Application.OnTime and Now have 1-second precision
Public Sub TimerTick()
    Dim TNext As Date
    'TNext = Now + Interval
    TNext = Date + Timer / 86400 + Interval
    
    If chkAnimationRun.Value <= 0 Then Exit Sub
    'DoEvents
    AnimationUpdate
    
    Dim T2 As Date
    T2 = Date + Timer / 86400
    If T2 > TNext Then TNext = T2 + 0.25 / 86400
    Application.OnTime TNext, "TimerTick"
End Sub

Sub SendAnimStop()
    If Not chkAnimationRun Is Nothing Then chkAnimationRun.Value = 0
    Application.OnKey "{ESCAPE}" 'Note: also add this line in Workbook_BeforeClose.
End Sub

Public Sub Animate()
    If AnimationActive Then Exit Sub
    On Error GoTo Finally
    Application.EnableCancelKey = xlErrorHandler
    Application.OnKey "{ESCAPE}", "SendAnimStop"
    AnimationActive = True
    
    'ThisWorkbook.Sheets("manager").EnableCalculation = False
    'ThisWorkbook.Sheets("world_vertices").EnableCalculation = False
    'ThisWorkbook.ForceFullCalculation = True
    Dim TNext As Double
    Do While chkAnimationRun.Value > 0
        TNext = Timer + Interval
        AnimationUpdate
        Do
            DoEvents
        Loop While Timer < TNext
    Loop
Finally:
    AnimationActive = False
    chkAnimationRun.Value = 0
    'ThisWorkbook.Sheets("manager").EnableCalculation = True
    'ThisWorkbook.Sheets("world_vertices").EnableCalculation = True
    'ThisWorkbook.ForceFullCalculation = False
    'If Err = 18 Then 'cancelled by user
    'Err.Raise
End Sub
