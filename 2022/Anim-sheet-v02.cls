VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit
Const UISheetName = "Sheet1"
Const BackendSheetName = "Sheet1"

Sub RecordKey(Key As String)
    mdlCalcPerf.LoadPerfFrequency
    Dim tblKeys As Name, Row As Long
    With ThisWorkbook.Sheets(BackendSheetName)
        Set tblKeys = .Names("tblKeyNames")
        'tblKeys.RefersToRange.Rows
        On Error GoTo Finally
        'alternative: https://learn.microsoft.com/en-us/office/vba/api/excel.range.find
        Row = WorksheetFunction.Match(Key, tblKeys.RefersToRange, 0)
        'tblKeys.RefersToRange.Cells(Row, 2).Value = Timer / 86400
        tblKeys.RefersToRange.Cells(Row, 2).Value = mdlCalcPerf.PerformanceCounter * 10000 * mdlCalcPerf.InvFreq
    End With
    'Exit Sub
Finally:
End Sub

Sub btnUpdate_Click()
    'Application.OnKey doesn't work well in this case.
    With ThisWorkbook
        .Activate
        
        'mdlAnimation.Interval = Val(Evaluate(.Names("UpdateInterval").Value)) / 86400
        mdlAnimation.Interval = Val(Evaluate(.Names("UpdateInterval").Value))
        Set mdlAnimation.NameT = .Names("AnimTick")
        mdlAnimation.T0 = Val(Evaluate(mdlAnimation.NameT.Value))
        mdlAnimation.Ticks = 0
        Set mdlAnimation.NameTimeCounter = .Names("TimeCounter")
        Set mdlAnimation.NameCalcTime = .Names("CalcTime")
        Set mdlAnimation.NameLastIdleCounter = .Names("LastIdleCounter")
        Set mdlAnimation.chkAnimationRun = .Worksheets(UISheetName).Shapes("chkAnimationRun").ControlFormat
    End With
    If mdlAnimation.chkAnimationRun.Value > 0 Then
        'mdlAnimation.TimerTick
        mdlAnimation.Animate
    Else
        mdlAnimation.AnimationUpdate
    End If
End Sub

Sub btnTimeReset_Click()
    With ThisWorkbook
        .Names("AnimTick").RefersTo = 0
        If .Worksheets(UISheetName).Shapes("chkAnimationRun").ControlFormat.Value <= 0 Then
            mdlAnimation.AnimationActive = False
        End If
    End With
End Sub

Private Sub TextBox1_Change() 'In deprecation
    'Sheet1.TextBox1.Text = Right(Sheet1.TextBox1.Text, 4)
    'RecordKey Right(Sheet1.TextBox1.Text, 1)
End Sub

Private Sub TextBox1_KeyDown(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If (Shift And &H4) <> 0 Then Exit Sub
    If (KeyCode >= KeyCodeConstants.vbKeyLeft) And (KeyCode <= KeyCodeConstants.vbKeyDown) Then
        Dim Char As String * 1
        Select Case KeyCode
            Case KeyCodeConstants.vbKeyLeft
                Char = "A"
            Case KeyCodeConstants.vbKeyUp
                Char = "W"
            Case KeyCodeConstants.vbKeyRight
                Char = "D"
            Case KeyCodeConstants.vbKeyDown
                Char = "S"
        End Select
        Sheet1.TextBox1.Text = Right(Sheet1.TextBox1.Text, 3) & Char
        RecordKey Char
    End If
End Sub

Private Sub TextBox1_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)

End Sub

Private Sub TextBox1_KeyUp(ByVal KeyCode As MSForms.ReturnInteger, ByVal Shift As Integer)
    If (Shift And &H4) <> 0 Then Exit Sub '? (might cause problems)

End Sub
