set dst=%~n1-256-g-i32.raw
rem magick %1 -define quantum:format=floating-point -depth 32 -resize 256 "gray:%dst%"
magick %1 -define quantum:format=unsigned -depth 32 -resize 256 "gray:%dst%"
rem certutil -encodehex -f "%dst%" "%dst%.txt" 12
timeout /t 10
