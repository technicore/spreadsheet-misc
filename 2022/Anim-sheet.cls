VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Option Explicit
Const UISheetName = "Sheet1"
Const BackendSheetName = "Sheet1"
Sub btnUpdate_Click()
    With ThisWorkbook
        .Activate
        
        'mdlAnimation.Interval = Val(Evaluate(.Names("UpdateInterval").Value)) / 86400
        mdlAnimation.Interval = Val(Evaluate(.Names("UpdateInterval").Value))
        Set mdlAnimation.NameT = .Names("AnimTick")
        mdlAnimation.T0 = Val(Evaluate(mdlAnimation.NameT.Value))
        mdlAnimation.Ticks = 0
        'Set mdlAnimation.NameOutput = .Names("OutputValue")
        Set mdlAnimation.NameCalcTime = .Names("CalcTime")
        Set mdlAnimation.chkAnimationRun = .Worksheets(UISheetName).Shapes("chkAnimationRun").ControlFormat
    End With
    If mdlAnimation.chkAnimationRun.Value > 0 Then
        'mdlAnimation.TimerTick
        mdlAnimation.Animate
    Else
        mdlAnimation.AnimationUpdate
    End If
End Sub

Sub btnTimeReset_Click()
    With ThisWorkbook
        .Names("AnimTick").RefersTo = 0
        If .Worksheets(UISheetName).Shapes("chkAnimationRun").ControlFormat.Value <= 0 Then
            mdlAnimation.AnimationActive = False
        End If
    End With
End Sub
