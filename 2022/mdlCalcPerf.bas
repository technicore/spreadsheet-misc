Attribute VB_Name = "mdlCalcPerf"
Option Explicit
'Based on code from: https://learn.microsoft.com/en-us/office/vba/excel/concepts/excel-performance/excel-improving-calculation-performance
'Notice: Many of the APIs invoked here originally use 64-bit integers.
'But for compatibility with 32-bit MS Office, those numbers go into a Currency-typed field, instead of Int64/LongLong (only available in 64-bit Office).
'Currency is 64-bit, but not an integer type, although still fixed-point. It uses a denominator of 10000.
'As a result, VBA reads the number in a distorted scale when converting to other types like Double. Therefore, we have to manually correct the scale.
Public cyFrequency As Currency, InvFreq As Double, Precision As Integer

#If VBA7 Then
    Private Declare PtrSafe Function QueryPerformanceFrequency Lib "Kernel32" (lpFrequency As Currency) As Boolean
    Private Declare PtrSafe Function QueryPerformanceCounter Lib "Kernel32" (lpPerformanceCount As Currency) As Boolean
    
    Private Declare PtrSafe Sub GetSystemTimeAsFileTime Lib "Kernel32" (lpSystemTimeAsFileTime As Currency)
    Private Declare PtrSafe Sub GetSystemTimePreciseAsFileTime Lib "Kernel32" (lpSystemTimeAsFileTime As Currency) 'Windows 8+/Windows Server 2012+ only
    Private Declare PtrSafe Function FileTimeToLocalFileTime Lib "Kernel32" (lpFileTime As Currency, lpLocalFileTime As Currency) As Boolean
    Private Declare PtrSafe Function LocalFileTimeToFileTime Lib "Kernel32" (lpLocalFileTime As Currency, lpFileTime As Currency) As Boolean
    
    Private Declare PtrSafe Function CloseHandle Lib "Kernel32" (ByVal hObject As LongPtr) As Boolean
    Private Declare PtrSafe Function GetCurrentProcess Lib "Kernel32" () As LongPtr
    Private Declare PtrSafe Function GetCurrentProcessId Lib "Kernel32" () As Long
    Private Declare PtrSafe Function OpenProcess Lib "Kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Boolean, ByVal dwProcessId As Long) As LongPtr
    Private Declare PtrSafe Function GetProcessTimes Lib "Kernel32" (ByVal hProcess As LongPtr, lpCreationTime As Currency, lpExitTime As Currency, lpKernelTime As Currency, lpUserTime As Currency) As Boolean
    
    Private Declare PtrSafe Function QueryProcessCycleTime Lib "Kernel32" (ByVal ProcessHandle As LongPtr, CycleTime As Currency) As Boolean 'Windows Vista+/Windows Server 2008+ only
    Private Declare PtrSafe Sub QueryInterruptTime Lib "Kernel32" (lpInterruptTime As Currency) 'Windows 10+/Windows Server 2016+ only
    Private Declare PtrSafe Sub QueryInterruptTimePrecise Lib "Kernel32" (lpInterruptTimePrecise As Currency) 'Windows 10+/Windows Server 2016+ only
    Private Declare PtrSafe Function QueryUnbiasedInterruptTime Lib "Kernel32" (UnbiasedTime As Currency) As Boolean '[sic] Windows 7+/8+/Windows Server 2008 R2+ only
    Private Declare PtrSafe Sub QueryUnbiasedInterruptTimePrecise Lib "Kernel32" (lpUnbiasedInterruptTimePrecise As Currency) 'Windows 10+/Windows Server 2016+ only
#Else
    Private Declare Function QueryPerformanceFrequency Lib "Kernel32" (lpFrequency As Currency) As Boolean
    Private Declare Function QueryPerformanceCounter Lib "Kernel32" (lpPerformanceCount As Currency) As Boolean
    
    Private Declare Sub GetSystemTimeAsFileTime Lib "Kernel32" (lpSystemTimeAsFileTime As Currency)
    Private Declare Sub GetSystemTimePreciseAsFileTime Lib "Kernel32" (lpSystemTimeAsFileTime As Currency) 'Windows 8+/Windows Server 2012+ only
    Private Declare Function FileTimeToLocalFileTime Lib "Kernel32" (lpFileTime As Currency, lpLocalFileTime As Currency) As Boolean
    Private Declare Function LocalFileTimeToFileTime Lib "Kernel32" (lpLocalFileTime As Currency, lpFileTime As Currency) As Boolean

    Private Declare Function CloseHandle Lib "Kernel32" (ByVal hObject As Long) As Boolean
    Private Declare Function GetCurrentProcess Lib "Kernel32" () As Long
    Private Declare Function GetCurrentProcessId Lib "Kernel32" () As Long
    Private Declare Function OpenProcess Lib "Kernel32" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Boolean, ByVal dwProcessId As Long) As Long
    Private Declare Function GetProcessTimes Lib "Kernel32" (ByVal hProcess As Long, lpCreationTime As Currency, lpExitTime As Currency, lpKernelTime As Currency, lpUserTime As Currency) As Boolean

    Private Declare Function QueryProcessCycleTime Lib "Kernel32" (ByVal ProcessHandle As Long, CycleTime As Currency) As Boolean 'Windows Vista+/Windows Server 2008+ only
    Private Declare Sub QueryInterruptTime Lib "Kernel32" (lpInterruptTime As Currency) 'Windows 10+/Windows Server 2016+ only
    Private Declare Sub QueryInterruptTimePrecise Lib "Kernel32" (lpInterruptTimePrecise As Currency) 'Windows 10+/Windows Server 2016+ only
    Private Declare Function QueryUnbiasedInterruptTime Lib "Kernel32" (UnbiasedTime As Currency) As Boolean '[sic] Windows 7+/8+/Windows Server 2008 R2+ only
    Private Declare Sub QueryUnbiasedInterruptTimePrecise Lib "Kernel32" (lpUnbiasedInterruptTimePrecise As Currency) 'Windows 10+/Windows Server 2016+ only
#End If

Sub LoadPerfFrequency()
    If cyFrequency <> 0 Then Exit Sub
    QueryPerformanceFrequency cyFrequency
    If cyFrequency <> 0 Then
        InvFreq = 0.0001 / cyFrequency
        Precision = Int(Log(cyFrequency) / Log(10) + 4 + 0.95) 'Currency numbers "hide" 4 decimal digits
    End If
End Sub

Function PerformanceFrequency() As Currency
    QueryPerformanceFrequency PerformanceFrequency
End Function

Function PerformanceCounter() As Currency
    QueryPerformanceCounter PerformanceCounter
End Function

Function PerformanceSeconds() As Double
    LoadPerfFrequency
    If cyFrequency = 0 Then
        PerformanceSeconds = 0
        Exit Function
    End If

    Dim cyTicks1 As Currency
    QueryPerformanceCounter cyTicks1
    PerformanceSeconds = cyTicks1 * 10000 * InvFreq
End Function

Function TimeCounter() As Double 'User-defined function for spreadsheets
    Application.Volatile
    LoadPerfFrequency
    TimeCounter = PerformanceCounter * 10000 * InvFreq
End Function

Function FileTimeAsCurrency() As Currency 'Recommended
    'GetSystemTimeAsFileTime FileTimeAsCurrency
    GetSystemTimePreciseAsFileTime FileTimeAsCurrency
End Function

Function FileTimeAsSecsDouble() As Double 'Returns seconds.
    Dim cyTicks1 As Currency
    'GetSystemTimeAsFileTime cyTicks1
    GetSystemTimePreciseAsFileTime cyTicks1
    FileTimeAsSecsDouble = cyTicks1 * 0.001 'Seconds
End Function

Public Function ProcessTimeSum() As Currency
    '#If Win64 Then
    '    Dim Handle As LongPtr
    '#Else
    '    Dim Handle As Long
    '#End If
    'Handle = OpenProcess(&H1FFFFF, False, GetCurrentProcessId())
    'Handle = OpenProcess(&H1400, False, GetCurrentProcessId())
    
    'Dim CreationTime As Currency, ExitTime As Currency
    Dim KernelTime As Currency, UserTime As Currency
    If GetProcessTimes(GetCurrentProcess(), 0, 0, KernelTime, UserTime) <> False Then
    'If GetProcessTimes(Handle, 0, 0, KernelTime, UserTime) <> False Then
        ProcessTimeSum = KernelTime + UserTime
    Else
        ProcessTimeSum = 0
    End If
    'If Handle <> 0 Then CloseHandle Handle
End Function

Public Function ProcessCycleTime() As Currency
    If QueryProcessCycleTime(GetCurrentProcess(), ProcessCycleTime) = False Then ProcessCycleTime = 0
End Function

Public Function InterruptTime() As Currency
    QueryInterruptTime InterruptTime
End Function

Public Function InterruptTimePrecise() As Currency
    QueryInterruptTimePrecise InterruptTimePrecise
End Function

Public Function InterruptTimeUnbiased() As Currency
    'Using the Not operator won't work.
    If QueryUnbiasedInterruptTime(InterruptTimeUnbiased) = False Then InterruptTimeUnbiased = 0
End Function

Public Function InterruptTimeUnbiasedPrecise() As Currency
    QueryUnbiasedInterruptTimePrecise InterruptTimeUnbiasedPrecise
End Function

Sub Timer_4Range()
    DoCalcTimer 1
End Sub

Sub Timer_3Sheet()
    DoCalcTimer 2
End Sub

Sub Timer_2Recalc()
    DoCalcTimer 3
End Sub

Sub Timer_1FullCalc()
    DoCalcTimer 4
End Sub

Sub TryPrintToShape(Output As String, ShapeName As String)
    On Error GoTo ErrHandl
    ActiveSheet.Shapes(ShapeName).TextFrame.Characters().Text = Output
    Exit Sub

ErrHandl:
    MsgBox Output, vbOKOnly + vbInformation, "CalcTimer"
End Sub

Sub DoCalcTimer(jMethod As Long)
    'Dim dTime As Double
    Dim ProcTimeSum As Currency, IntTimeUnbiased As Currency
    Dim PerfCounter As Currency ', FileTime As Currency
    Dim ProcCycles As Currency
    Dim oRng As Range, oRngOld As Range, oCell As Range, oArrRange As Range
    Dim sCalcType As String
    Dim lCalcSave As Long
    Dim bIterSave As Boolean
    
    On Error GoTo ErrHandl

    'Initialize
    LoadPerfFrequency
    'dTime = PerformanceSeconds()

    'Save calculation settings.
    'Application.ScreenUpdating = False
    lCalcSave = Application.Calculation
    bIterSave = Application.Iteration
    If (lCalcSave <> xlCalculationManual) And (jMethod <= 2) Then Application.Calculation = xlCalculationManual
    
    Select Case jMethod
        Case 1
            'Switch off iteration.
            If bIterSave <> False Then Application.Iteration = False
            
            'Max is used range.
            Set oRng = Selection
            If oRng.Count > 1000 Then Set oRng = Intersect(oRng, oRng.Parent.UsedRange)
    
            'Include array cells outside selection.
            Set oRngOld = oRng
            For Each oCell In oRngOld
                If oCell.HasArray Then
                    If oArrRange Is Nothing Then
                        Set oArrRange = oCell.CurrentArray
                        Set oRng = Union(oRng, oArrRange)
                    Else
                        If Intersect(oCell, oArrRange) Is Nothing Then
                            Set oArrRange = oCell.CurrentArray
                            Set oRng = Union(oRng, oArrRange)
                        End If
                    End If
                End If
            Next oCell

            sCalcType = "Calculate " & CStr(oRng.Count) & " cell(s) in selected range (iteration off)"
        Case 2
            sCalcType = "Recalculate sheet " & ActiveSheet.Name
        Case 3
            sCalcType = "Recalculate " & CStr(Workbooks.Count) & " open workbook(s)"
        Case 4
            sCalcType = "Full-calculate " & CStr(Workbooks.Count) & " open workbook(s)"
    End Select

    'Get start time.
    ProcTimeSum = ProcessTimeSum()
    IntTimeUnbiased = InterruptTimeUnbiased()
    'dTime = PerformanceSeconds()
    PerfCounter = PerformanceCounter()
    'FileTime = FileTimeAsCurrency()
    ProcCycles = ProcessCycleTime()
    
    Select Case jMethod
        Case 1
            If Val(Application.Version) >= 12 Then
                oRng.CalculateRowMajorOrder
            Else
                oRng.Calculate
            End If
        Case 2
            ActiveSheet.Calculate
        Case 3
            Application.Calculate
        Case 4
            Application.CalculateFull
    End Select

    'Calculate duration.
    ProcTimeSum = ProcessTimeSum() - ProcTimeSum
    IntTimeUnbiased = InterruptTimeUnbiased() - IntTimeUnbiased
    'dTime = PerformanceSeconds() - dTime
    PerfCounter = PerformanceCounter() - PerfCounter
    'FileTime = FileTimeAsCurrency() - FileTime
    ProcCycles = ProcessCycleTime() - ProcCycles
    On Error GoTo 0

    'dTime = Round(dTime, Precision)
    Dim Status As String
    Status = sCalcType & ":" & vbCrLf _
        & "Process time (kernel + user): " & CStr(ProcTimeSum) & " ms" & vbCrLf _
        & "Unbiased interrupt time: " & CStr(IntTimeUnbiased) & " ms" & vbCrLf _
        & "Performance counter: " & CStr(10000000 * CDbl(PerfCounter) * InvFreq) & " ms" & vbCrLf _
        & "(Counter frequency: " & CStr(cyFrequency * 0.01) & " MHz)" & vbCrLf _
        & "Process CPU cycles: " & Format$(ProcCycles * 10000, "0.0###e-0")
        '& "Process CPU cycles: " & FormatNumber(ProcCycles * 10000, 0, vbTrue, vbFalse, vbTrue)
    If bIterSave Then
        Status = Status & vbCrLf & "(Iteration: up to " & CStr(Application.MaxIterations) & " step(s))"
    Else
        Status = Status & vbCrLf & "(Iteration: off)"
    End If
    'MsgBox Status, vbOKOnly + vbInformation, "CalcTimer"
    TryPrintToShape Status, "lblPerformance"

Finish:
    'Restore calculation settings.
    'Application.ScreenUpdating = True
    If Application.Iteration <> bIterSave Then Application.Iteration = bIterSave
    If Application.Calculation <> lCalcSave Then Application.Calculation = lCalcSave
    Exit Sub
    
ErrHandl:
    On Error GoTo 0
    MsgBox "Unable to calculate: " & sCalcType, vbOKOnly + vbCritical, "CalcTimer"
    GoTo Finish
End Sub
