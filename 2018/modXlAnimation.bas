Attribute VB_Name = "Module1"
Dim Interval As Date
Dim T0 As Double
Dim Ticks As Integer
Dim NameT, NameOutput As Name
Dim chkAnimationRun As ControlFormat

Sub AnimationUpdate()
    NameOutput.RefersTo = Rnd
    Ticks = Ticks + 1
    NameT.RefersTo = T0 + Ticks
End Sub

Public Sub TimerTick()
    Dim T1 As Date
    T1 = Now + Interval
    
    If chkAnimationRun.Value <= 0 Then
        Exit Sub
    End If
    DoEvents
    AnimationUpdate
    
    If Now > T1 Then T1 = Now
    Application.OnTime T1, "TimerTick"
End Sub
